package com.test.takeHome;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.tomcat.util.json.JSONParser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class ServerConfigurationService {

	@GetMapping("/serverConfig")
	Map<String, Object> getServerConfig()throws FileNotFoundException {
		
		  try {
			  File configFile=getConfigFile();
	    	  JSONParser parser = new JSONParser(new FileReader(configFile));
	    	  return parser.parseObject();
	    	  
	      } catch(Exception e) {
	         e.printStackTrace();
	      }
		return null;
	}

	@PutMapping("/serverConfig")
	Map<String, Object> replaceServerConfig(@RequestBody Map<String, String> body) throws Exception {
		
		File configFile=getConfigFile();
		JSONParser parser = new JSONParser(new FileReader(configFile));
		
		LinkedHashMap <String,Object> serverConfigkeyVals= parser.parseObject();
		boolean dirty=false;
		
		
		for (Entry<String,String>  configKeyVal: body.entrySet()) {

			if ("host".equalsIgnoreCase(configKeyVal.getKey()) || "port".equalsIgnoreCase(configKeyVal.getKey()))
			{
				continue;
				
			}
			
			if (serverConfigkeyVals.containsKey(configKeyVal.getKey()))
				{
				dirty=true;
				serverConfigkeyVals.put(configKeyVal.getKey(), configKeyVal.getValue());
				}
			
			
		}
		
		if (!dirty)
		{
			throw new Exception ("Nothing changed");
		}
		
		
		writeConfigFile(serverConfigkeyVals);
		
		return serverConfigkeyVals;
		
		
		
	
	}

	public File getConfigFile() throws FileNotFoundException {

		File file = new File(this.getClass().getClassLoader().getResource(".").getFile() + "/config.json");
		if (file.exists()) {
			return file;

		} else {
			throw new FileNotFoundException("File doesnt exists, please check ur configuration.,");
		}

	}
	public void writeConfigFile(LinkedHashMap <String,Object> configs) throws Exception {

		File file =getConfigFile();
		FileWriter writer=new FileWriter(file);
		
		ObjectMapper mapper = new ObjectMapper();
	   writer.write(mapper.writeValueAsString(configs));
		
	
	}
	
	
	
	
	

}

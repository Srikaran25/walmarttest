package com.test.takeHome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TakeHomeExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(TakeHomeExerciseApplication.class, args);
	}

}
